#include <stdio.h>
#define SIZE 6

void poliDeri(const int poli[], unsigned n, int deri[]);
void printPoli(const int poli[], unsigned n);

int main(void){
	int polinomio[SIZE] = {1, 3, 4, 0, 0, 2};
	int derivada[SIZE - 1];

	puts("Polinomio:");
	printPoli(polinomio, SIZE);
	poliDeri(polinomio, SIZE, derivada);

	puts("");
	puts("Derivada:");
	printPoli(derivada, SIZE - 1);

	return 0;
}


void poliDeri(const int poli[], unsigned n, int deri[]){
	for (unsigned i = 0; i < n - 1; i++){
		deri[i] = (i + 1) * poli[ i + 1];
	}
}

void printPoli(const int poli[], unsigned n){
	printf("%s", "coeficientes |");

	for (unsigned i = 0; i < n; i++){
		printf("%4d |", poli[i]);
	}

	printf("\n%s", "índices      |");

	for (unsigned i = 0; i < n; i++){
		printf("%4d |", i);
	}

	puts("");
}
