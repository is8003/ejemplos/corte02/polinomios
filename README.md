# Polinomios[^1]

[^1]: Punto 2 del parcial 2017-30: <https://sophia.javeriana.edu.co/programacion/sites/default/files/pdfParciales/2017-30-PensamientoAlgoritmico-2.pdf>

Un polinomio de una variable es una expresión matemática constituída por una
suma finita de productos de una variable con valores constantes (o
coeficientes). Estos productos también se conocen como monomios. La variable
puede tener diferentes exponentes enteros positivos, cuyo valor máximo
determina el grado del polinomio. Matemáticamente, se puede expresar así:

```math
P(x) = \sum_{i=0}^{n}a_ix^i
```

donde $`x`$ es la variable que define el polinomio, y las constantes $`a_0,
..., a_n`$ son los coeficientes del polinomio.

Computacionalmente, el polinomio de una variable puede almacenarse como un
vector de coeficientes, donde el índice del coeficiente indica el exponente de
la variable al que acompaña. Ejemplo: el polinomio $`P(x) =2x^5 + 4x^2 + 3x +
1`$ se almacenaría así:

```text
coeficientes | 1 | 3 | 4 | 0 | 0 | 2 |
índices      | 0 | 1 | 2 | 3 | 4 | 5 |
```

Por otro lado, el concepto de derivada de un polinomio se puede definir como la
suma de las derivadas de cada uno de los términos del polinomio. La derivada de
un valor constante es cero, mientras que la derivada de un monomio como
$`ax^r`$ es $`(a \cdot r)x^{r−1}`$. Matemáticamente, para el polinomio
completo se expresar así:

```math
P'(x) = \sum_{i=1}^{n}(i \cdot a_i)x^{i-1}
```

Ejemplo: para el polinomio $`P(x) = 2x^5 + 4x^2 + 3x + 1`$, su derivada es
$`P′(x) = 10x^4 + 8x + 3`$, la cual computacionalmente se almacenaría así:

```text
coeficientes | 3 | 8 | 0 | 0 | 10 |
índices      | 0 | 1 | 2 | 3 |  4 |
```

Teniendo en cuenta esto, proponga una función que reciba un vector (que
representa un polinomio) con su tamaño, y genere un nuevo vector que contenga
la derivada del polinomio de entrada.

## Refinamiento 1

1. Función recibe vector, tamaño de vector y arreglo adicional para almacenar
   derivada (referencia). 
1. Recorro arreglo desde `0` hasta tamaño menos `2`:
   1. Asigno valores a arreglo de salida como $`i \cdot a_i`$ en un subíndice
      menos, donde $`i`$ es el subíndice y $`a_i`$ es el coefiente del vector
      de entrada.

## Refinamiento 2

### Función: `poliDeri`

Genera derivada de un polinomio usando vectores.

<div class="center">

```mermaid
graph TD
    ini(("Inicio"))-->setI["i = 0"]
    setI-->iCond{"i < n - 1"}
    iCond--"Falso"-->fin(("Fin"))
    iCond--"Verdadero"-->setDeri["deri[i] = (i + 1) * poli[i + 1]"]
    setDeri-->iInc["i++"]
    iInc-->iCond
```
</div>

#### Entradas:

- `poli`: vector que contiene el polinomio.
- `n`: tamaño del vector.
- `deri`: vector sobre el que se genera la derivada del polinomio.

#### Salidas:

- Ninguna (referencia).

#### Pseudo-código:

1. Para `i` desde `0`; hasta `i` menor a `n - 1`; incremento `i` en `1`:
   1. Asigno a `deri[i]` el valor de `(i + 1) * poli[i + 1]`.

# Referencias
