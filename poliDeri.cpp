#include <iostream>
#include <iomanip>
#define SIZE 6

using namespace std;

void poliDeri(const int poli[], unsigned n, int deri[]);
void printPoli(const int poli[], unsigned n);

int main(void){
	int polinomio[SIZE] = {1, 3, 4, 0, 0, 2};
	int derivada[SIZE - 1];

	cout << "Polinomio:" << endl;
	printPoli(polinomio, SIZE);
	poliDeri(polinomio, SIZE, derivada);

	cout << endl << "Derivada:" << endl;
	printPoli(derivada, SIZE - 1);

	return 0;
}


void poliDeri(const int poli[], unsigned n, int deri[]){
	for (unsigned i = 0; i < n - 1; i++){
		deri[i] = (i + 1) * poli[ i + 1];
	}
}

void printPoli(const int poli[], unsigned n){
	cout << "coeficientes |";

	for (unsigned i = 0; i < n; i++){
		cout << setw(4) << poli[i] << " |";
	}

	cout << endl << "índices      |";

	for (unsigned i = 0; i < n; i++){
		cout << setw(4) << i << " |";
	}

	cout << endl;
}
